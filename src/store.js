import { createStore } from 'redux';

const initialState = {
  displayList: true,
  value: '',
  todos: [
    {
      id: 0,
      name: 'Description 1 Redux',
      done: false,
    },
    {
      id: 1,
      name: 'Description 2 Redux',
      done: false,
    },
  ],
  todoUpdate: [],
};

const reducerTodo = (state = initialState, action) => {

  if (action.type === 'Add_Item') {
    console.log(action.value);
    return {
      ...state,
      todos: state.todos.concat({
        id: state.todos.length + 1,
        name: action.value,
        done: false,
      }),
      value:'',
    };
  }

  if (action.type === 'text_Value') {
    console.log('text value');
    return {
      ...state,
      value: action.value,
    };
  }

  if (action.type === 'remove_Item') {
    console.log('remove_Item');
    return {
      ...state,
      todos: state.todos.filter((t) => action.id !== t.id),
      todoUpdate: state.todoUpdate.filter((t) => action.id !== t.id),
    };
  }
  if (action.type === 'pending') {
    console.log('pending');
    return {
      ...state,
      displayList: false,
      todoUpdate: state.todos.filter(t => t.done === false),
    };
  }
  if (action.type === 'done') {
    console.log('done');
    return {
      ...state,
      displayList: false,
      todoUpdate: state.todos.filter(t => t.done === true),
    };
  }
  if (action.type === 'all') {
    console.log('all');
    return {
      ...state,
      displayList: true,
    };
  }
  if (action.type === 'update_Status') {
    console.log('update_Status', action.status);
    console.log('update_Status', action.status.id);
    console.log('update_Status', !action.status.done);
  
    return {
      ...state,
      todos: state.todos.map(todo => todo.id === action.status.id ?
          // transform the one with a matching id
          { ...todo, done: !action.status.done } : 
          // otherwise return original todo
          todo
      ) 
  };

  }
  return state;
};

export default createStore(reducerTodo);
