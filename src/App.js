import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import TodoList from './componnets/TodoList';
import Styles from './App.module.scss';

const App = () => (
  <Provider store={store}>
    <div className={`${Styles['main-wrapper-app']} `}>
      <div className={`${Styles['main-wrapper-app--row-center']} `}>
        <TodoList />
      </div>
    </div>
  </Provider>
);
export default App;
