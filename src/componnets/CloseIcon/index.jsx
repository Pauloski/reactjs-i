import React from 'react';
import Proptypes from 'prop-types';
import styles from './CloseIcon.module.scss';

const CloseIcon = ({ action }) => (

    <div className={`${styles.close} `} onClick={action} />


);

CloseIcon.propTypes = {
  action: Proptypes.func,
};

CloseIcon.defaultProps = {
  action: Proptypes.func,
};

export default CloseIcon;
