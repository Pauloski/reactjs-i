import React from 'react';
import Proptypes from 'prop-types';
import Styles from './RadioButton.module.scss';


const RadioButton = ({
  Name,
  TextOption,
  Option,
  Id,
  Disabled,
  action,
  checked,
}) => (
  <div className={`${Styles.radio} `} onChange={action}>
    <label htmlFor={Id}>
      { Disabled === true ?
        <input type="radio" value={Option} name={Name} id={Id} disabled />
        : checked === true ?
          <input type="radio" value={Option} name={Name} id={Id} defaultChecked="checked" />
          : <input type="radio" value={Option} name={Name} id={Id}  />}
      <span>{ TextOption }</span>
    </label>

  </div>
);

RadioButton.propTypes = {
  Name: Proptypes.string,
  TextOption: Proptypes.string,
  Option: Proptypes.string,
  Id: Proptypes.string,
  action: Proptypes.func,
  // eslint-disable-next-line react/require-default-props
  Disabled: Proptypes.bool,
  checked: Proptypes.bool,

};
RadioButton.defaultProps = {
  Name: 'RadioButton',
  TextOption: 'RadioButton Component',
  Option: 'RadioButton',
  Id: 'RadioButton',
  action: Proptypes.func,
  // eslint-disable-next-line react/default-props-match-prop-types
  Disable: false,
  checked: false,
};

export default RadioButton;
