import React from 'react';
import Proptypes from 'prop-types';
import Style from './HelloWord.module.scss';

const HelloWord = ({ name }) => (
  <>
    <div className={`${Style['hello-word-wrapper']}`}>
      <h1>
        Hello
        { name }
      </h1>
    </div>
  </>
);

HelloWord.propTypes = {
  name: Proptypes.string,
};

HelloWord.defaultProps = {
  name: 'Matías',
};

export default HelloWord;
