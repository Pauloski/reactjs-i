import React, { Component } from 'react';
import Button from '../Button';
import Title from '../Title';
import Counter from '../CounternNumber';
import Styles from './CounterApp.module.scss';

export default class CounterApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
    };
  }

  increment = () => {
    console.log('Hola +1');
    const { counter } = this.state;
    this.setState({ counter: counter + 1 });
  }


  decrement = () => {
    console.log('Hola -1');
    this.setState((event) => ({ counter: event.counter - 1 }));
  }

  render() {
    const { counter } = this.state;
    return (

      <>
        <div className={`${Styles['counter-wrapper']} `}>
          <Title text="Counter App" weight="bold" align="center" fontSize="gamma" />
          <Counter number={counter} />
          <Button textButton="Suma 1" action={this.increment} type="pill" />
          <Button textButton="Resta 1" action={this.decrement} />
        </div>
      </>
    );
  }
}

/*
export default CounterApp;
*/
