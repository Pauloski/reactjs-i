/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import Title from '../Title';
import InputText from '../InputText';
import ListItem from '../ListItem';
import PlusButton from '../PlusButton';
import RadioButton from '../RadioButton';
import Styles from './TodoList.module.scss';


class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

onChangeList = (e) => {
  this.setState({ value: e.target.value });
  // eslint-disable-next-line react/destructuring-assignment
  console.log('value', this.state.value);
}

clearInput = (e) => {
  if(e === '') {
    this.setState({ value: '' });
  }
}


render() {
  const { value } = this.state;
  const {
    todos,
    todoUpdate,
    displayList,
    addItem,
    removeItem,
    updateStatus,
    pending,
    done,
    all,
  } = this.props;
  return (
    <>
      <div className={`${Styles['todo-list']} `}>
        <div className={`${Styles['todo-list__wrapper-title']} `}>
          <Title text="Lista de tareas" weight="bold" align="center" fontSize="gamma" />
        </div>
        <PlusButton textButton="Nueva tarea" action={() => { addItem(value); this.clearInput(''); }} />
        <InputText action={(e) => this.onChangeList(e)} value={value} inputName="addItem" textLabel="Agregar tareas" />
        <div className={`${Styles['todo-list__wrapper-filter']} `}>
          <RadioButton Name="radioData" TextOption="Ver todos" Option="val1" Id="radio1" action={() => all()} checked={displayList} />
          <RadioButton Name="radioData" TextOption="Pendientes" Option="val2" Id="radio2" action={() => pending()} />
          <RadioButton Name="radioData" TextOption="Terminados" Option="val3" Id="radio3" action={() => done()} />
        </div>

        {
          (displayList === true ? todos : todoUpdate).map((item, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={index} className={item.done !== true ? `${Styles['todo-list__item-not-done']} not-done ` : `${Styles['todo-list__item-done']} done `}>
              <ListItem
                name={item.name}
                id={item.id}
                done={item.done}
                remove={() => removeItem(item.id)}
                checkbox={() => updateStatus(item)}
              />
            </div>
          ))
        }
      </div>
    </>
  );
}
}


const mapStateToProps = (state) => ({
  todos: state.todos,
  todoUpdate: state.todoUpdate,
  displayList: state.displayList,
});

const mapDispatchToProps = (dispatch) => ({
  addItem(value) {
    dispatch({
      type: 'Add_Item',
      value,
    });
  },
  removeItem(id) {
    dispatch({
      type: 'remove_Item',
      id,
    });
  },
  addType(value) {
    dispatch({
      type: 'Typing',
      value,
    });
  },
  updateStatus(status) {
    dispatch({
      type: 'update_Status',
      status,
    });
  },
  pending() {
    dispatch({
      type: 'pending',
    });
  },
  done() {
    dispatch({
      type: 'done',
    });
  },
  all() {
    dispatch({
      type: 'all',
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
