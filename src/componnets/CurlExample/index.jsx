import React, { Component } from 'react';
import Title from '../Title';
import InputText from '../InputText';
import Button from '../Button';
import ListItem from '../ListItem';
import Styles from './CurlExample.module.scss';

export default class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // eslint-disable-next-line react/no-unused-state
      filter: '',
      // eslint-disable-next-line react/no-unused-state
      todos: [
        {
          id: 1,
          name: 'Description 1',
          done: false,
        },
        {
          id: 2,
          name: 'Description 2',
          done: true,
        },
      ],
    };
  }

  render() {
    const { todos, value } = this.state;
    return (

      <>
        <div className={`${Styles['todo-list']} `}>
          <Title text="Todo List" weight="bold" align="center" fontSize="gamma" />
          <InputText action={this.onChangeList} value={value} inputName="addItem" textLabel="Agregar tareas" />
          <Button textButton="Nueva tarea" type="pill" action={this.addItem} />
          <Button textButton="Buscar" type="pill" action={this.searchItem} />
          {
          todos.map((item, index) => (
            <div key={index} className={`${Styles['todo-list__item']} `}>
              <ListItem name={item.name} id={item.id} done={item.done} />
            </div>
          ))
        }
  </div>
  </>
);
}
}
