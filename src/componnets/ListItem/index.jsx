/* eslint-disable react/prop-types */
import React from 'react';
import Checkbox from '../Checkbox';
import Style from './ListItem.module.scss';
import CloseIcon from '../CloseIcon';

const ListItem = ({
  id,
  name,
  done,
  remove,
  checkbox,
}) => (
  <>
    <div data-testid="todo-item" className={`${Style['list-item']} ${Style[`list-item--done-${done}`]} `}>
      <span className={`${Style['list-item--checkbox']} `}>
        <Checkbox id={id} done={done} action={checkbox} />
      </span>
      <span className={`${Style['list-item--name']}  ${Style[`list-item--done-text-${done}`]}`}>
        { name }
      </span>
      <span className={`${Style['list-item--status']} `}>
        {done === true
          ? (
            <span className={`${Style['list-item--done']}`}>
              <img src={require('../../assets/images/done.png')} alt="status done" />
              Completado
            </span>
          )
          : (
            <span className={`${Style['list-item--active']}`}>
              <img src={require('../../assets/images/wip.png')} alt="status wip" />
              Pendiente
            </span>
          )}
      </span>

      <span className={`${Style['list-item--delate']} `}>
        <CloseIcon action={remove} />
      </span>

    </div>

  </>
);


export default ListItem;
