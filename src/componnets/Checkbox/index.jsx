import React from 'react';
import Proptypes from 'prop-types';
import Styles from './Checkbox.module.scss';


const Checkbox = ({ done, id, action }) => (
  <>
    {done === true ? (
      <input
        type="checkbox"
        id={`checkbox${id}`}
        className={`${Styles['option-input']}`}
        value={done}
        onChange={action}
        checked
      />
    ) : (
      <input
        type="checkbox"
        id={`checkbox${id}`}
        className={`${Styles['option-input']}`}
        value={done}
        onChange={action}
      />
    )}
  </>
);

Checkbox.propTypes = {
  done: Proptypes.bool,
  id: Proptypes.number,
  action: Proptypes.func,

};
Checkbox.defaultProps = {
  done: false,
  id: 0,
  action: Proptypes.func,
};


export default Checkbox;
