import React from 'react';
import Proptypes from 'prop-types';
import styles from './InputText.module.scss';

const defaultFunc = () => {
  console.log('default function InputText');
};

const InputText = ({ action, value, textLabel, inputName }) => (

  <label htmlFor={inputName} className={`${styles.todo} `}>
    <input
      type="text"
      name={inputName}
      id={inputName}
      value={value}
      onChange={action}
      placeholder="&nbsp;"
    />
    <span className={`${styles['todo--label']} `}>{textLabel}</span>
    <span className={`${styles['todo--border']} `} />
  </label>

);

InputText.propTypes = {
  action: Proptypes.func,
  value: Proptypes.string,
  textLabel: Proptypes.string,
  inputName: Proptypes.string,
};

InputText.defaultProps = {
  action: defaultFunc,
  value: '',
  textLabel: 'input text default',
  inputName: 'default',
};

export default InputText;
