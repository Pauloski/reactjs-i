import React from 'react';
import Proptypes from 'prop-types';
import './Button.scss';
import styles from './Button.module.scss';


const Button = ({ textButton, action, type }) => (

  <>
    <button className={`${styles[`mybutton--${type}`]} `} type="button" onClick={action}>
      { textButton }
    </button>
  </>
);

Button.propTypes = {
  textButton: Proptypes.string,
  action: Proptypes.func,
  type: Proptypes.oneOf(['pill', 'pill-outline', 'pill-disable', 'minimize', 'send', 'add-tocard', 'pill-outline-width', 'pill-width']),
};

Button.defaultProps = {
  textButton: 'Boton',
  action: Proptypes.func,
  type: 'pill',
};

export default Button;
