import React from 'react';
import Proptypes from 'prop-types';
import './Button.scss';
import styles from './PlusButton.module.scss';

const PlusButton = ({ textButton, action }) => (
  <>
    <button 
      className={`${styles['icon-btn']} ${styles['add-btn']} `}
      onClick={action}
    >
    <div className={`${styles['add-icon']}`}></div>
    <div className={`${styles['btn-txt']}`}>{ textButton }</div>
   </button>
   
  </>
);

PlusButton.propTypes = {
  textButton: Proptypes.string,
  action: Proptypes.func,
};

PlusButton.defaultProps = {
  textButton: 'Boton',
  action: Proptypes.func,
};

export default PlusButton;
