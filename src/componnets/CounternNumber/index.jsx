import React from 'react';
import Proptypes from 'prop-types';

const CounternNumber = ({ number }) => (
  <>
    <h3>
      { number }
    </h3>
  </>
);

CounternNumber.propTypes = {
  number: Proptypes.number,
};

CounternNumber.defaultProps = {
  number: 0,
};

export default CounternNumber;
